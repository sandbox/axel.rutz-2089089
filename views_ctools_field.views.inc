<?php

/**
 * @file views_ctools_field.views.inc
 * Implement views specific hooks.
 */

/**
 * Implements hook_views_data().
 *
 * Adds global panel-panes that take no context
 */
function views_ctools_field_views_data() {
  ctools_include('plugins');
  ctools_include('context');

  $data = array();

  // Psuedo table for global content type that have no required context
  $data['ctools_field_global']['table']['group'] = t('Panel Pane - Global');
  $data['ctools_field_global']['table']['join'] = array(
    '#global' => array(),
  );

  // For each plugin, define a 'column' on a psudo-table
  $plugins = ctools_get_plugins('ctools', 'content_types');
  foreach ($plugins as $plugin_name => $plugin) {

    // Determine the psuedo-table based on context
    if (!isset($plugin['required context'])) {
      $psuedo_table = '';
      $real_field = NULL;

      $data['ctools_field_global'][$plugin_name] = array(
        'title' => $plugin['title'],
        'help' => isset($plugin['description']) ? $plugin['description'] : '',
        'field' => array(
          'handler' => 'views_ctools_field_handler_field',
          'click sortable' => FALSE,
          'type' => FALSE,
          'ctools_plugin_name' => $plugin_name,
        ),
      );
    }
  }

  return $data;
}

/**
 * Implements hook_views_data_alter().
 *
 * Adds entity context panel-panes to each entity base table
 */
function views_ctools_field_views_data_alter(&$data) {
  ctools_include('context');
  ctools_include('plugins');

  $entity_info = entity_get_info();
  $plugins = ctools_get_plugins('ctools', 'content_types');

  //@@TODO: expand subtypes
  foreach ($plugins as $plugin_name => $plugin) {
    if (isset($plugin['required context'])) {

      // Determine the correct context
      if (is_array($plugin['required context'])) {
        foreach ($plugin['required context'] as $check_context) {
          if ($check_context->required) {
            $context = $check_context;
            break;
          }
        }
      }
      else {
        $context = $plugin['required context'];
      }

      // If there is no valid context, skip this plugin
      if (!$context) {
        continue;
      }

      // Grab the keywords for the contet, we use these to match to entity types
      if (is_string($context->keywords)) {
        $keywords = array($context->keywords);
      }
      else if (is_array($plugin['required context']->keywords)) {
        $keywords = $context->keywords;
      }
      else {
        continue;
      }

      foreach ($keywords as $keyword) {
        if (isset($entity_info[$keyword])) {
          $entity_type = $keyword;

          $field_def = array(
            'title' => $plugin['title'],
            'help' => isset($plugin['description']) ? $plugin['description'] : '',
            'field' => array(
              'handler' => 'views_ctools_field_handler_field',
              'click sortable' => FALSE,
              'ctools_plugin_name' => $plugin_name,
              'type' => $entity_type,
              'real field' => $entity_info[$entity_type]['entity keys']['id'],
            ),
            'group' => t('Panel Pane') . ' - ' . $entity_info[$entity_type]['label'],
          );

          // Add this field to the base table for the entity
          $base_table = $entity_info[$entity_type]['base table'];
          $data[$base_table]['ctools_field_' . $plugin_name] = $field_def;

          // Add it to entity field query table if the module is enabled
          if (module_exists('efq_views')) {
            $data['efq_' . $entity_type]['ctools_field_' . $plugin_name] = $field_def;
          }

          break;
        }
      }
    }
  }
}
